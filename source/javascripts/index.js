import 'babel-polyfill'

import Vue from 'vue'
import Application from './Application.vue'

Vue.config.productionTip = false

new Vue( {
  render: h => h( Application )
} ).$mount( '#application' )
