# August - Activity three.

### A live example of this project can be found at: https://august-3.netlify.com/

In order to accomplish this I rolled in Vue for all of the DOM related shenanigans. I would typically roll in Lodash for reasoning about the model mutations, but just went with vanilla JS for the most part.

## Setup

This project uses [Parcel](https://parceljs.org/) for local development and production builds. It's spec'd as a dev dependency so doesn't need to be installed globally. The following should be enough to get the project up and running locally.

```javascript
// Install dependencies
npm i

// Run locally
npm run develop
```
