const variables = {
  'color-theme-primary': '#003049',
  'color-error': '#f9dfdf',
  'color-info': '#ebeff0',
  'color-loading': '#e3effd',
  'color-success': '#e2f6d9'
}

const extensions = {
  '--palm': '(max-width: 749px)',
  '--lap-down': '(max-width: 999px)',
  '--lap': '(min-width: 750px)',
  '--lap-only': '(min-width: 750px) and (max-width: 999px)',
  '--desk-down': '(max-width: 1499px)',
  '--desk': '(min-width: 1000px)',
  '--desk-only': '(min-width: 1000px) and (max-width: 1499px)',
  '--wall': '(min-width: 1500px)'
}

module.exports = {
  plugins: [
    require( 'autoprefixer' )( {
      grid: true
    } ),
    require( 'postcss-nested' ),
    require( 'postcss-custom-properties' )( {
      variables,
      preserve: 'computed'
    } ),
    require( 'postcss-custom-media' )( {
      extensions
    } ),
    require( 'postcss-color-function' ),
    require( 'postcss-position' ),
    require( 'postcss-size' )
  ]
}
